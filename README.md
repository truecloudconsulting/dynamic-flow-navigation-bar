# How to setup simple dynamic navigation bar buttons #

Using the components in this repo you'll be able to control navigation button visibility and access to end users. 
There is no apex code. Just a simple lightning web component.


### How do I get set up? ###

Just copy the LWC from this repo into your org. It is built to only be available to Flows via Flow Builder.

Another option is to install via this unmanaged package - https://login.salesforce.com/packaging/installPackage.apexp?p0=04t5g0000008xsQ&isdtp=p1

### Basic use - overriding standard navigation labels ###

For any screen that you want more granular control of navigation, just disable the standard footer and replace with this component. Then work
through the configurable options to set new labels.

The component will inherit whatever nav button options you've set for the standard footer, however you are also offered the options of overriding the 
standard labels and setting the buttons to disabled.  The first configurable option is obvious - you can make the labels more contextual then 'Back' and 'Next'. 
The additional options to disable a button means you can create dynamic button enable/disable scenarios.

### More advanced use - creating dynamic enable/disable nav buttons ###

Using the disable button options means you can more visually prevent users from progressing via the 'Next' button until they've completed all form fields or selected an option etc.
Just drop two components onto the flow screen and make them the same with exception of button being disabled or not. Then use the conditional visibility to toggle view for each
component - the component with disabled button visible if criteria not yet met, and then the component with active button once criteria have been met.

