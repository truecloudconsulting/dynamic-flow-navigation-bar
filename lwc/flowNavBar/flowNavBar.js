import { api, LightningElement, track } from 'lwc';
import { FlowNavigationNextEvent, 
         FlowNavigationBackEvent, 
         FlowNavigationPauseEvent, 
         FlowNavigationFinishEvent } from 'lightning/flowSupport';

export default class flowNavBar extends LightningElement {
    @api availableActions = []; 
    @api previousLabel;
    @api nextLabel;
    @api pauseLabel;
    @api finishLabel;
    @track showFinishButton = false;
    @track showNextButton = false;
    @track showPreviousButton = false;
    @track showPauseButton = false;
    
    // only assesses what should be displayed once the rendering cycle is complete and availableActions populated 
    renderedCallback() {
        console.log('actions=',JSON.stringify(this.availableActions));
        this.showNextButton = (this.availableActions.find(action => action === 'NEXT') === 'NEXT');
        this.showPreviousButton = (this.availableActions.find(action => action === 'BACK') === 'BACK');
        this.showFinishButton = (this.availableActions.find(action => action === 'FINISH') === 'FINISH');
        this.showPauseButton = (this.availableActions.find(action => action === 'PAUSE') === 'PAUSE');
    }
    
    handleNextAction(){
        this.dispatchEvent(new FlowNavigationNextEvent());
    }

    handlePreviousAction(){
        this.dispatchEvent(new FlowNavigationBackEvent());
    }

    handlePauseAction(){
        this.dispatchEvent(new FlowNavigationPauseEvent());
    }

    handleFinishAction(){
        this.dispatchEvent(new FlowNavigationFinishEvent());
    }
}